import java.util.ArrayList;

public abstract class Perfil {
 
// ATRIBUTOS DO PERFIL
 
 //Nome do Usuario
 private String usuario;

 //Usuarios seguidos por esse perfil
 private ArrayList<Perfil> seguidos;

 //Usuarios que seguem este perfil
 private ArrayList<Perfil> seguidores;

 //Tweets deste perfil e dos perfis seguidos por ele
 private ArrayList<Tweet> timeline;

 //Status do perfil
 private boolean ativo;

//=================================================


// METODO CONSTRUTOR
 public Perfil(String usuario){
    
    //Configuração Inicial do perfil
    this.usuario = usuario;
    seguidos = new ArrayList<Perfil>();
    seguidores = new ArrayList<Perfil>();
    timeline = new ArrayList<Tweet>();
    ativo = true;

 }

 //Metodo responsável por adicionar um seguidor ao perfil do usuário
 public void addSeguidor(Perfil usuario){
    seguidores.add(usuario);
 }

//Metodo responsável por adicionar uma pessoa que está sendo seguida pelo usuário ao perfil deste
 public void addSeguido(Perfil usuario){
    seguidos.add(usuario);
 }

//Adiciona novo Tweet a timeline do usuário
 public void addTweet(Tweet tweet){
    timeline.add(tweet);

    for( Perfil seguidor : seguidores ) {
       seguidor.timeline.add(tweet);
    }
 }

//Muda o nome de usuario deste perfil
 public void setUsuario(String usuario){
    this.usuario = usuario;
 }

//Retorna o nome do usuario deste perfil
 public String getUsuario(){
    return usuario;
 }

//Retorna os seguidores deste perfil
 public ArrayList<Perfil> getSeguidores(){
    return seguidores;
 }

//Retorna os perfis seguidos por esse usuario
 public ArrayList<Perfil> getSeguidos(){
    return seguidos;
 } 

//Retorna os tweets criados por esse usuário e os tweets dos usuarios que esse usuario segue
 public ArrayList<Tweet> getTimeline(){
    return timeline;
 }

//Muda o status do usuario de ativo para inativo e vice versa
 public void setAtivo(boolean valor){
    ativo = valor;
 }

//Retorna o status do usuario(ativo ou inativo)
 public boolean isAtivo() {
    return ativo;
 }

} 