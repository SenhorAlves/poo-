import java.util.ArrayList;

public class PessoaFisica extends Perfil {
 
 //ATRIBUTO DE PESSOA FISICA
 private long cpf;

//METODO CONSTRUTOR
 public PessoaFisica(String usuario){
     super(usuario);
 }

//Configura o cpf do perfil de pessoa fisica
 public void setCpf(long cpf){
     this.cpf = cpf;
 }

//Retorna o cpf do perfil de pessoa fisica
 public long getCpf(){
    return cpf;
 }

}