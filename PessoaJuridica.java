import java.util.ArrayList;

public class PessoaJuridica extends Perfil {
 
//ATRIBUTO DE PESSOA JURIDICA
 private long cnpj;

//METODO CONSTRUTOR
 public PessoaJuridica(String usuario){
    super(usuario);
 }

//Configura o cnpj do perfil de pessoa juridica
 public void setCnpj(long cnpj){
     this.cnpj = cnpj;
 }

//Configura o cnpj do perfil de pessoa juridica
 public long getCnpj(){
     return cnpj;
 }

}