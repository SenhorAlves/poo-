public class Tweet {

//ATRIBUTOS DO TWEET

//nome do usuario que fez o tweet
 private String usuario;

//mensagem feita no tweet
 private String mensagem;

//===============================

//METODO CONSTRUTOR
 public Tweet(String usuario){
     this.usuario = usuario;
 }

//configura quem foi o usuario que fez o tweet
 public void setUsuario(String usuario){
     this.usuario = usuario;
 }

//retorna o usuario responsavel por gerar este tweet
 public String getUsuario(){
     return usuario;
 }

//determina qual mensagem sera gravado no tweet
 public void setMensagem(String mensagem){
     this.mensagem = mensagem;
 }

//retorna a mensagem cadastrada no tweet
 public String getMensagem(){
     return mensagem;
 }

}